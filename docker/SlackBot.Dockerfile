FROM node:11-alpine
RUN apk add gcc make libc-dev python3 g++
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN ln -s /usr/bin/pip3 /usr/bin/pip
WORKDIR /app