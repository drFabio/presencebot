const sqlite3 = require("sqlite3").verbose();

class DbManager {
  constructor() {}
  async init(dbFile = "/data/mainDb.sqlite3") {
    this._db = new sqlite3.Database(dbFile);
    this._createTables();
  }
  _createTables() {
    this._createClass();
    this._createPresence();
  }

  _createClass() {
    const sql = `
        CREATE TABLE IF NOT EXISTS classes (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            channel_id TEXT UNIQUE,
            teacher_id TEXT,
            teacher_name TEXT,
            duration integer,
            start_time TEXT
        )
    `;
    return this._db.run(sql);
  }
  _createPresence() {
    const sql = `
        CREATE TABLE IF NOT EXISTS presence (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            class_name,
            class_channel_id,
            student_id TEXT,
            student_name TEXT,
            date TEXT
        )
    `;
    return this._db.run(sql);
  }
  startClass({ teacher_id, teacher_name, channel_name, channel_id, duration }) {
    const sql = `
      INSERT INTO classes (
        name,
        channel_id,
        teacher_id,
        teacher_name,
        duration,
        start_time
      )
      VALUES(?, ? ,?, ?, ? ,datetime('now'))
    `;
    const stmt = this._db.prepare(sql);
    stmt.run(channel_name, channel_id, teacher_id, teacher_name, duration);
  }
}

module.exports = DbManager;
