const getSlackEvents = require("./getSlackEvents");
const getServer = require("./getServer");
const setupCommands = require("./setupCommands");
const ClassManager = require("./ClassManager");
const MessageHandler = require("./MessageHandler");
const DbManager = require("./DbManager");

(async () => {
  if (!process.env.BOT_TOKEN || !process.env.SIGNIN_SECRET) {
    console.log(`Tokens are not set, exiting`);
    process.exit(1);
  }
  const dbManager = new DbManager();
  await dbManager.init();
  console.log("Sucessfully initialized the db");

  const messageHandler = new MessageHandler(process.env.BOT_TOKEN);
  const classManager = new ClassManager(messageHandler, dbManager);
  await classManager.init();
  const slackEvents = getSlackEvents();
  const server = getServer(slackEvents);
  setupCommands(server, classManager);
})();
