const request = require("request");
const { WebClient } = require("@slack/web-api");

class MessageHandler {
  constructor(botToken) {
    this._botToken = botToken;
    this._web = new WebClient(botToken);
  }
  async getMembersOfChannel({ channel }) {
    try {
      const response = await this._web.conversations.members({ channel });
      return response.members;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
  async sendEphemeral({ channel, recipient, text, attachments = [] }) {
    console.log(JSON.stringify({ attachments }));
    try {
      const response = await this._web.chat.postEphemeral({
        channel,
        text,
        attachments,
        user: recipient
      });
      console.log(response);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
  async getUsersList() {
    const url = `https://slack.com/api/users.list?token=${this._botToken}`;
    return new Promise((resolve, reject) => {
      request(url, (error, response, reqBody) => {
        if (error) {
          reject(error);
        } else {
          resolve({ response, reqBody });
        }
      });
    });
  }
  async answerAction({ response_url, channel, text, ts, attachments = [] }) {
    const payload = {
      channel,
      text,
      replace_original: "true",
      ts
    };
    const options = {
      uri: response_url,
      method: "POST",
      json: {
        ...payload
      }
    };
    return new Promise((resolve, reject) => {
      request(options, (error, response, reqBody) => {
        if (error) {
          reject(error);
        } else {
          resolve({ response, reqBody });
        }
      });
    });
  }

  async getMemberInfo(userId) {
    return this._web.users.info({
      user: userId
    });
  }
}
module.exports = MessageHandler;
