/**
 *
 * @param {*} app
 * @param {ClassManager} classManager
 */
function setupCommands(app, classManager) {
  app.post("/commands/startClass", (req, res) => {
    classManager.startClass(req, res);
  });
  app.post("/commands/slowDown", async (req, res) => {
    await classManager.slowDown(req, res);
  });
  app.post("/commands/speedUp", async (req, res) => {
    await classManager.speedUp(req, res);
  });
  app.post("/commands/takePresence", async (req, res) => {
    await classManager.takePresence(req, res);
  });
  app.post("/slack/actions", async (req, res) => {
    await classManager.handleAction(req, res);
  });
}
module.exports = setupCommands;
