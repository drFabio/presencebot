const express = require("express");
const bodyParser = require("body-parser");

/**
 *
 * @param {slackEvents} slackEvents the slack events to listen to
 * @param {number} [port=3000]
 */
function getServer(slackEvents, port = 3000) {
  const app = express();
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.use("/slack/events", slackEvents.requestListener());
  app.get("/status", async (req, res) => {
    res.send("I am alive!");
  });
  app.listen(port, () => console.log(`Example app listening on port ${port}!`));
  return app;
}

module.exports = getServer;
