const START_CLASS_REGEX = /.*(hours=(\d+)).*/;
const TAKE_PRESENCE_REGEX = /.*(number=(\d+)).*/;

const CLASS_TOKEN = "rediFall2019";
const PRESENCE_CALLBACK_ID = "register_presence";
class ClassManager {
  constructor(messageHandler, dbManager) {
    this._messageHandler = messageHandler;
    this._dbManager = dbManager;
    this._openClasses = {};
    this._usersTeamMap = {};
  }
  async init() {
    // const { response } = await this._messageHandler.getUsersList();
    // const { statusCode } = response;
    // if (statusCode !== 200) {
    //   throw new Error(`User list status code not 200 , it was ${statusCode} `);
    // }
    // console.log("#############################");
    // const parsedBody = JSON.parse(response.body);
    // const { members = [] } = parsedBody;
    // members.forEach(member => {
    //   const { team_id } = member;
    //   if (!this._usersTeamMap.hasOwnProperty(team_id)) {
    //     this._usersTeamMap[team_id] = {};
    //   }
    //   this._usersTeamMap[team_id] = member;
    // });
    // console.log(members);
    // console.log("#############################");
  }
  startClass({ body }, res) {
    const { channel_id, user_id, user_name, text } = body;

    console.log("on start class of data", body);
    if (text.indexOf(CLASS_TOKEN) === -1) {
      res.send(`You are missing a class token! try again!`).status(200);
      return;
    }
    let hours = 2;
    if (text) {
      const match = START_CLASS_REGEX.exec(text);
      console.log({ match });
      if (match && match[2]) {
        hours = Math.max(8, parseInt(match[2], 10));
      }
    }
    setTimeout(() => this.endClass(channel_id), 1000 * 60 * 60 * hours);
    this._openClasses[channel_id] = {
      data: body,
      hours,
      teacher: {
        id: user_id,
        name: user_name
      }
    };
    res.send(`Ok I will start this class for ${hours} hours!`).status(200);
  }
  _getCurrentClass(channelId, res) {
    if (!this._openClasses.hasOwnProperty(channelId)) {
      res
        .send("Can't find this class, ask your teacher to start it!")
        .status(404);
      throw new Error(`Class on ${channelId} not found`);
    }
    return this._openClasses[channelId];
  }
  endClass() {
    if (this._openClasses.hasOwnProperty(channelId)) {
      delete this._openClasses[channelId];
    }
  }
  async slowDown({ body }, res) {
    const { channel_id, user_id } = body;
    const currentClass = this._getCurrentClass(channel_id, res);
    const { teacher } = currentClass;
    const slowdownText = `The student <@${user_id}> asked to slowdown ;)`;
    try {
      console.log(
        JSON.stringify({
          channel: channel_id,
          recipient: teacher.id,
          text: slowdownText
        })
      );
      await this._messageHandler.sendEphemeral({
        channel: channel_id,
        recipient: teacher.id,
        text: slowdownText
      });
      res.send(`Ok we will tell the teacher to slowdown a bit`).status(200);
    } catch (e) {
      res.send(`Something went wrong`).status(500);
    }
  }

  async speedUp({ body }, res) {
    const { channel_id, user_id } = body;
    const currentClass = this._getCurrentClass(channel_id, res);
    const { teacher } = currentClass;
    const slowdownText = `The student <@${user_id}> asked to speed up a bit, KEEP GOING!`;
    try {
      await this._messageHandler.sendEphemeral({
        channel: channel_id,
        recipient: teacher.id,
        text: slowdownText
      });
      res.send(`Ok we will tell the teacher to speed up a bit`).status(200);
    } catch (e) {
      res.send(`Something went wrong`).status(500);
    }
  }
  async handleAction({ body }, res) {
    console.log(` ON HANDLE ACTION \n\n\t ${JSON.stringify(body, null, 4)}`);
    const payload = JSON.parse(body.payload);
    switch (payload.callback_id) {
      case PRESENCE_CALLBACK_ID:
        await this._handlePresenceResponse(payload, res);
        break;
    }
    res.send().status(200);
  }
  async takePresence({ body }, res) {
    const { channel_id, user_id, text: incomingText } = body;
    const currentClass = this._getCurrentClass(channel_id, res);

    const teachersList = [currentClass.teacher.id];
    if (!teachersList.includes(user_id)) {
      res
        .send(`You are not a teacher on this channel! You can't take presence`)
        .status(403);
      return;
    }
    let number = Math.floor(Math.random() * 9 + 1);
    if (incomingText) {
      const match = TAKE_PRESENCE_REGEX.exec(incomingText);
      if (match && match[2]) {
        number = Math.min(10, parseInt(match[2], 10));
      }
    }
    currentClass.presenceNumber = parseInt(number, 10);
    currentClass.presenceMap = {};
    const allMembers = await this._messageHandler.getMembersOfChannel({
      channel: channel_id
    });
    const students = allMembers.filter(i => {
      const isStudent = !teachersList.includes(i);
      if (isStudent) {
        currentClass.presenceMap[i] = { present: false };
      }
      return isStudent;
    });
    res.send(
      `We will ask the students what is the magic number. It is  -> ${number} `
    );
    const attachments = [
      {
        text: "What is the number for the presence",
        fallback:
          "The teacher asked for your presence Say to him the magic number.",
        color: "#3AA3E3",
        attachment_type: "default",
        callback_id: PRESENCE_CALLBACK_ID,
        actions: [
          {
            name: "presence_number",
            text: "Select the rignt number...",
            type: "select",
            options: Array(10)
              .fill(0)
              .map((val, index) => ({ text: index, value: index }))
          }
        ]
      }
    ];
    const text = `Are you in class?`;
    students.forEach(student => {
      try {
        this._messageHandler.sendEphemeral({
          channel: channel_id,
          recipient: student,
          text,
          attachments
        });
      } catch (error) {}
    });
  }
  async _handlePresenceResponse(payload, res) {
    console.log(`HANDLIGN PRESENCE`);
    const { channel, user, actions, message_ts, response_url } = payload;
    const channel_id = channel.id;
    const answer = actions[0].selected_options[0].value;
    const currentClass = this._getCurrentClass(channel_id, res);
    const { teacher, presenceMap, presenceNumber } = currentClass;
    const userInfo = await this._messageHandler.getMemberInfo(user.id);
    console.log(JSON.stringify({ userInfo }));
    const realName = userInfo.user.profile.real_name_normalized;
    presenceMap[user.id].name = realName;
    let text;
    if (presenceNumber !== parseInt(answer, 10)) {
      text = `Wrong number (${answer}), your presence is not registered`;
      this._messageHandler.answerAction({
        channel: channel_id,
        text,
        ts: message_ts,
        response_url
      });
      return;
    }
    text = `Correct number (${answer}), your presence is registered! Thanks for coming!`;
    this._messageHandler.answerAction({
      channel: channel_id,
      text,
      ts: message_ts,
      response_url
    });
    presenceMap[user.id].present = true;
    const presentUsers = Object.values(presenceMap).filter(
      ({ present }) => present
    );
    if (presentUsers.length) {
      await this._messageHandler.sendEphemeral({
        channel: channel_id,
        recipient: teacher.id,
        text: `The following students are present \n ${presentUsers
          .map(({ name }) => name)
          .join(", ")}`
      });
    }
  }
}
module.exports = ClassManager;
