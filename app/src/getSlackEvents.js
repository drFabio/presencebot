const { createEventAdapter } = require("@slack/events-api");

function getSlackEvents() {
  const slackEvents = createEventAdapter(process.env.SIGNIN_SECRET);
  slackEvents.on("message", event => {
    console.log(
      `Received a message event: user ${event.user} in channel ${event.channel} says ${event.text}`
    );
  });
  slackEvents.on("reaction_added", event => {
    console.log("Reaction event received");
    console.log(event, respond);
    // Normal success
  });

  slackEvents.on("message", event => {
    console.log(
      `Received a message event: user ${event.user} in channel ${event.channel} says ${event.text}`
    );
  });
  slackEvents.on("reaction_added", event => {
    console.log("Reaction event received");
    console.log(event, respond);
    // Normal success
  });
  return slackEvents;
}
module.exports = getSlackEvents;
